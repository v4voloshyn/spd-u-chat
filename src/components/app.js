import './app.css';

import {Header} from './header/header';
import {Main} from './main/main';
import React from 'react';

export const App = () => {
        
    return (
    <div className="app">
        <Header/>
        <Main/>
    </div>
)};
