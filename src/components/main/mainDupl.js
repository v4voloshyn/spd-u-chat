import './main.css';

import React, {useEffect, useRef, useState} from 'react';

import {INITIAL_MESSAGES} from './constants';
import {Message} from './message/message';

export const Main = () => {
    const [messages, setMessages] = useState([]);
    const [currentMessage, setCurrentMessage] = useState('');

    const socket = useRef();
    socket.current = new WebSocket('ws://localhost:8080');

    const onTextAreaChange = ({target: {value}}) => {
        setCurrentMessage(value);
    };

    const sendMessage = (e) => {
        e.preventDefault();
        
        if (!currentMessage.trim()) {
            return
        }
        socket.current.send(JSON.stringify({ text:currentMessage, user: Date.now()}))
        setCurrentMessage('');
    };

    useEffect(() => {
        const messageHandler = (e) => {
            console.log('event data', JSON.parse(e.data))
            const msg = JSON.parse(e.data);
            setMessages((messages) => ([...messages, {text: msg.text, isCurrentUser: true}])) 
        }

        socket.current.onopen = () => {
            const userName = 'Anonymous';
            
            console.log(`${userName} successfully connected!`);
        }
            
        socket.current.onerror = () => {
            console.log(`There something wrong with WebSocket connection...`);
        }
        socket.current.onclose = () => {
            console.log(`Connection is closed!`);
        }
        socket.current.addEventListener('message', messageHandler);
        
        return () => {
            socket.current.removeEventListener('message', messageHandler)
        }
    },[])
    
    return (
        <div className="main">
            <div className="main__messages">
                {messages.map(({text, isCurrentUser}, i) => (
                    <Message key={i} text={text} isCurrentUser={isCurrentUser}/>
                ))}
            </div>
            <form onSubmit={sendMessage}>
                <div className="main__plate">
                    <textarea className="main__textarea" onChange={onTextAreaChange} value={currentMessage}/>
                </div>
                <div className="main__plate">
                    <button className="main__button" type='submit'>Send message</button>
                </div>
            </form>
        </div>
    )
};
