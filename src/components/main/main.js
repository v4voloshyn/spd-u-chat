import './main.css';

import React, { useEffect, useState } from 'react';
import { sendMessage, startMessagesListening, stopMessagesListening } from '../../redux/slice/chat-slice';
import { useDispatch, useSelector } from 'react-redux';

import {Message} from './message/message';
import { nanoid } from 'nanoid';

export const Main = () => {
    const [currentMessage, setCurrentMessage] = useState('');
    const [currentUser, setCurrentUser] = useState({name: 'Anonimous', id: null})
    const {messages} = useSelector(state => state.chat);
    const dispatch = useDispatch();

    const onTextAreaChange = ({target: {value}}) => {
        setCurrentMessage(value);
    };

    const sendMessageHandler = (e) => {
        e.preventDefault();
        
        if (!currentMessage.trim()) {
            return
        }
        dispatch(sendMessage({ text:currentMessage, from: currentUser}))
        setCurrentMessage('');
    };

    useEffect(() => {
                dispatch(startMessagesListening())
        return () => {
            dispatch(stopMessagesListening());
        }
    }, []);

    useEffect(() => {
        let userData = JSON.parse(localStorage.getItem('chat-user'));

        if(userData && userData.id) {
            setCurrentUser(userData);
        } else {
            const userName = window.prompt('Name?');
            userData = { name: userName , id: nanoid()};
            setCurrentUser(userData);
            localStorage.setItem('chat-user', JSON.stringify(userData));
        }
    }, []);
    
    return (
        <div className="main">
            <div className="main__messages">
                {messages && messages.map((props, i) => {
                    const {text, from} = props;
                    return (
                        <Message key={i} text={text} isCurrentUser={from.id === currentUser.id}
                        />
                    )
                })}
            </div>
            <form onSubmit={sendMessageHandler}>
                <div className="main__plate">
                    <textarea className="main__textarea" onChange={onTextAreaChange} value={currentMessage}/>
                </div>
                <div className="main__plate">
                    <button className="main__button" type='submit'>Send message</button>
                </div>
            </form>
        </div>
    )
};
