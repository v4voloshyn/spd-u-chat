const { configureStore } = require("@reduxjs/toolkit");
const { rootReducer } = require("./root-reducer");

export const store = configureStore({
	reducer: rootReducer,
	devTools: process.env.NODE_ENV !== 'production'
})