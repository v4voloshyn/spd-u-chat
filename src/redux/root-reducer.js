import { chatSlice } from './slice/chat-slice'
import { combineReducers } from '@reduxjs/toolkit'
import { userSlice } from './slice/user-slice'

export const rootReducer = combineReducers({
	chat: chatSlice.reducer,
	user: userSlice.reducer
})