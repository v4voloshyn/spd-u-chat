import { createAsyncThunk, createSlice } from "@reduxjs/toolkit"

import { chatAPI } from "../../api/chat-api";
import { nanoid } from "nanoid"

const initialState = {
	data: {
		name: null, 
		id: null
	},
	status: 'ready',
	errorMessage: '',
}


export const userSlice = createSlice({
	name: 'userSlice',
	initialState,
	reducers: {
		userCreated: (state, action) => {
			console.log('action.payload', action.payload)
			state.data.push({
				name: action.payload, 
				id: nanoid(),
			})
		}
	},
	extraReducers: {}
})

export const {userCreated} = userSlice.actions;