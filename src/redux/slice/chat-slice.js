import { createAsyncThunk, createSlice } from "@reduxjs/toolkit"

import { chatAPI } from "../../api/chat-api"

const initialState = {
	messages: [],
	status: 'ready',
	errorMessage: '',
}

let _newMessageHandler = null;
const newMessageHandlerCreator = (dispatch) => {
	if(_newMessageHandler === null) {
		_newMessageHandler = (messages) => {
			dispatch(messagesReceived(messages))
		}
	}
	return _newMessageHandler
}

export const startMessagesListening = createAsyncThunk('chatSlice/startListening', (_, {dispatch}) => {
	chatAPI.start();
	chatAPI.subscribe(newMessageHandlerCreator(dispatch));
});

export const stopMessagesListening = createAsyncThunk('chatSlice/stopListening', (_, {dispatch}) => {
	chatAPI.unsubscribe(newMessageHandlerCreator(dispatch));
	chatAPI.stop();
});

export const sendMessage = createAsyncThunk('chatSlice/sendMessage', (message) => {
	chatAPI.sendMessage(message);
});

export const chatSlice = createSlice({
	name: 'chatSlice',
	initialState,
	reducers: {
		messagesReceived: (state, action) => {
			state.messages.push(action.payload)
		}
	},
	extraReducers: (builder) => 
		builder
			.addCase(startMessagesListening.pending, (state) => {
					state.status = 'loading';
					state.errorMessage = '';
				})
				.addCase(startMessagesListening.fulfilled, (state) => {
					state.status = 'ready';
					state.errorMessage = '';
				})
				.addCase(startMessagesListening.rejected, (state, action) => {
					state.status = 'error';
					state.errorMessage = action.payload;
				}),
})

export const {messagesReceived} = chatSlice.actions;