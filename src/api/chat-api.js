
let subscribers = [];
let ws = null;

const closeHandler = () => {
	console.log(`Closed WS`);
	setTimeout(createChannel, 3000);
}

const messageHandler = (e) => {
	const newMessage = JSON.parse(e.data);
	subscribers.forEach(s => s(newMessage));
}

function createChannel(){
	removeListeners()
	ws?.close();
	ws = new WebSocket('ws://localhost:8080');
	ws?.addEventListener('close', closeHandler);
	ws?.addEventListener('message', messageHandler);
	console.log('Connected by SET TIMEOUT!');
}

function removeListeners() {
	ws?.removeEventListener('close', closeHandler);
	ws?.removeEventListener('message', messageHandler);
}

export const chatAPI = {
	subscribe(cb) {
		subscribers.push(cb)
	},
	unsubscribe(cb) {
		subscribers.filter(subscriber => subscriber !== cb );
	},
	sendMessage(message) {
		ws?.send(JSON.stringify(message));
	},
	start() {
		createChannel();
	},
	stop() {
		subscribers = [];
		removeListeners();
		ws?.close();
	}
}