

const WebSocket = require('ws');

const wss = new WebSocket.Server({ port: 8080 }, () => console.log('Server is running on port 8080'));


wss.on('connection', function connection(ws) {
    ws.on('message', function (rawMessage) { 
        const message = JSON.parse(rawMessage);
        broadcastSending(message);
    });
});

function broadcastSending(message) {
    wss.clients.forEach(client => { // current online clients
        client.send(JSON.stringify(message));
    }) 
}


